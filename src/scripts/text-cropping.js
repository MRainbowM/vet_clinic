let listBox = document.querySelectorAll('.cropping')

function main() {
    listBox.forEach((box) => {
        let text = box.innerHTML
        let clone = document.createElement('div')
        clone.style.position = 'absolute'
        clone.style.visibility = 'hidden'
        clone.style.width = box.clientWidth + 'px'
        clone.innerHTML = text
        document.body.appendChild(clone);

        let l = text.length - 1
        for (; l >= 0 && clone.clientHeight > box.clientHeight; l--) {
            clone.innerHTML = text.substring(0, l) + '...'
        }
        box.innerHTML = clone.innerHTML
    })
}
function onResize() {
    if (listBox) {
        main()
    }
}
if (listBox) {
    main()
}
window.addEventListener('resize', onResize)
// window.setCarusel = setCarusel