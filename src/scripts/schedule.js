let root = document.querySelector('.schedule') //корн эл
let grid = root && root.querySelector('.schedule-grid__body')
let viewWeek = 1
let currentDate = new Date()

function cleanGrid() {
    grid.innerHTML = ''
}

function setViewWeek() {
    if (window.innerWidth <= 600) {
        viewDays = 1
    }
    else {

        viewDays = getWeeks(year, month)
    }
}

function twoLen(time) {
    let timeTwo = ''
    // console.log((time + '').length )
    if ((time + '').length == 1) {
        timeTwo = '0' + time
    }
    else {
        timeTwo = time
    }
    return timeTwo
}

function renderDays(year, month, tasks) {
    let elem = grid
    let firstDay = new Date(year, month, 1).getDay()
    let daysInMonth = 32 - new Date(year, month, 32).getDate()
    let l = new Date(year, month + 1, 0)
    let weekInMonth =  Math.ceil((l.getDate() - (l.getDay() ? l.getDay() : 7)) / 7 ) + 1
    let htmlStr = ''
    let lastDay = new Date(year, month, daysInMonth).getDay()
    let countEmptyCell = 0
    if (firstDay == 0) {
        countEmptyCell = 6
    } else {
        countEmptyCell = firstDay - 1
    }
    for (let i = 0; i < countEmptyCell; i++) {
        htmlStr += `<div class="cell empty"></div>`
    }
    for (let i = 1; i <= daysInMonth; i = i + 1) {
        let task = tasks.find(el => new Date(el.from).getFullYear() === year && new Date(el.from).getMonth() === month && new Date(el.from).getDate() === i)
        let d = new Date(year, month, i).getDay()
        let weekNames = ['ВС', 'ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ']
        if (task) {
            let fh = twoLen(new Date(task.from).getHours())
            let fm = twoLen(new Date(task.from).getMinutes())
            let th = twoLen(new Date(task.to).getHours())
            let tm = twoLen(new Date(task.to).getMinutes())
            if (task.filial == 1) {
                htmlStr +=`<div class="cell cell__weekName">${weekNames[d]}</div>`
                htmlStr +=`<div class="cell green"><div class="cell__dom">${i}</div><span>${fh}:${fm} - ${th}:${tm}</span></div> `
            }
            if (task.filial == 2) {
                htmlStr +=`<div class="cell  cell__weekName">${weekNames[d]}</div>`
                htmlStr +=`<div class="cell blue"><div class="cell__dom">${i}</div><span>${fh}:${fm} - ${th}:${tm}</span></div> `
            }
            if (task.filial == 3) {
                htmlStr +=`<div class="cell  cell__weekName">${weekNames[d]}</div>`
                htmlStr += `<div class="cell orange"><div class="cell__dom">${i}</div><span>${fh}:${fm} - ${th}:${tm}</span></div> `
            }
            if (task.filial == 4) {
                htmlStr +=`<div class="cell  cell__weekName">${weekNames[d]}</div>`
                htmlStr +=`<div class="cell yellow"><div class="cell__dom">${i}</div><span>${fh}:${fm} - ${th}:${tm}</span></div> `
            }
        }
        else {
            htmlStr +=`<div class="cell  cell__weekName">${weekNames[d]}</div>`
            htmlStr +=`<div class="cell"><div class="cell__dom">${i}</div></div>`
        }
    }  
    for (let i = 0; i < 7 - lastDay; i++) {
        // htmlStr +=`<div class="cell  cell__weekName">${weekNames[d]}</div>`
        htmlStr += `<div class="cell empty"></div>`
    }

    elem.innerHTML = htmlStr
}

function main() {
    let tasks = [
        {
            from: new Date(2019, 9, 1, 9, 0).getTime(),
            to: new Date(2019, 9, 1, 17, 0).getTime(),
            filial: 1
        },
        {
            from: new Date(2019, 9, 2, 9, 0).getTime(),
            to: new Date(2019, 9, 2, 17, 0).getTime(),
            filial: 1
        },
        {
            from: new Date(2019, 9, 3, 9, 0).getTime(),
            to: new Date(2019, 9, 3, 17, 0).getTime(),
            filial: 3
        },
        {
            from: new Date(2019, 9, 4, 9, 0).getTime(),
            to: new Date(2019, 9, 4, 17, 0).getTime(),
            filial: 3
        },
        {
            from: new Date(2019, 9, 5, 9, 0).getTime(),
            to: new Date(2019, 9, 5, 17, 0).getTime(),
            filial: 3
        },
        {
            from: new Date(2019, 9, 15, 9, 0).getTime(),
            to: new Date(2019, 9, 15, 17, 0).getTime(),
            filial: 3
        },
        {
            from: new Date(2019, 9, 17, 9, 0).getTime(),
            to: new Date(2019, 9, 17, 17, 0).getTime(),
            filial: 4
        },
        {
            from: new Date(2019, 9, 19, 9, 0).getTime(),
            to: new Date(2019, 9, 19, 17, 0).getTime(),
            filial: 1
        },
        {
            from: new Date(2019, 9, 20, 9, 0).getTime(),
            to: new Date(2019, 9, 20, 17, 0).getTime(),
            filial: 4
        },
        {
            from: new Date(2019, 9, 27, 9, 0).getTime(),
            to: new Date(2019, 9, 27, 17, 0).getTime(),
            filial: 2
        },
        {
            from: new Date(2019, 9, 29, 9, 0).getTime(),
            to: new Date(2019, 9, 29, 17, 0).getTime(),
            filial: 1
        },
        {
            from: new Date(2019, 9, 30, 9, 0).getTime(),
            to: new Date(2019, 9, 30, 17, 0).getTime(),
            filial: 2
        }
    ]
    renderDays(2019, 9, tasks)
}

if (root) {
    main()
}

// console.log(new Date)