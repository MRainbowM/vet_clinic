function onInit() {
    let lists = document.querySelectorAll('[data-carusel]')
    lists.forEach((list) => {
        let section = list.parentElement.parentElement
        let button = section.querySelector('button[onclick]')
        setCarusel(button, 0)
        list.addEventListener('touchstart', onTouchStart)
        list.addEventListener('touchend', onTouchEnd)
    })
}

function onResize() {
    let lists = document.querySelectorAll('[data-carusel]')
    lists.forEach((list) => {
        let section = list.parentElement.parentElement
        let button = section.querySelector('button[onclick]')
        setCarusel(button, 0)
    })
}

let touchStartX = 0
let touchMinDist = 30

function onTouchStart(e) {
    touchStartX = e.changedTouches[0].pageX
} 

function onTouchEnd(e) {
    let distX = e.changedTouches[0].pageX - touchStartX
    let absDistX = Math.abs(distX)
    let distOk = (absDistX > touchMinDist)
    if (distOk) {
        let touchDirect = (distX < 0) ? 'left' : 'right'
        touchSetCarucel(e, touchDirect)
    }
} 

function touchSetCarucel(e, direct) {
    let list = e.target.closest('[data-carusel]')
    if (parent) {
        let step = (direct === 'left') ? 1 : (direct === 'right') ? -1 : 0
        let section = list.parentElement.parentElement
        let button = section.querySelector('button[onclick]')
        setCarusel(button, step)
    }
}

function setCarusel(button, step) {
    let section = button.parentElement
    let buttons = section.querySelectorAll('button[onclick]')
    let list = section.querySelector('[data-carusel]')
    if (list.firstElementChild) {
        let lenght = list.childElementCount
        let w = list.firstElementChild.offsetWidth + 10
        let viewLenght = Math.floor(list.offsetWidth / w)
        let count = lenght - viewLenght
                            // console.log({list, buttons, lenght, viewLenght, count})
        let index = +(list.dataset.carusel) + step
        if(index >= 0 && index <= count) {
            list.style.transform = `translateX(calc(${index} * ${w}px * -1))`
            list.dataset.carusel = index
        }
        if(index <= 0)  {
            buttons[0].style.opacity = 0        
            buttons[0].style.pointerEvents = 'none'        
        } else {
            buttons[0].style.opacity =  1       
            buttons[0].style.pointerEvents = null        
        }
        if(index >= count) {
            buttons[1].style.opacity = 0        
            buttons[1].style.pointerEvents = 'none'        
        } else {
            buttons[1].style.opacity =  1       
            buttons[1].style.pointerEvents = null        
        }
    } else {
        buttons[0].style.opacity = 0        
        buttons[0].style.pointerEvents = 'none'
        buttons[1].style.opacity = 0        
        buttons[1].style.pointerEvents = 'none'
    }
}

onInit()
window.addEventListener('resize', onResize)
window.setCarusel = setCarusel
