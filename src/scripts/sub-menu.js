let buttons = document.querySelectorAll('.m-sub-menu, .m-sub-menu__close')
let menu = document.querySelector('.page__sub-menu')

const onClick = (e) => {
    // debugger
    let isHide = menu.classList.contains('hide')
    if(isHide) {
        menu.classList.remove('hide')
        menu.classList.add('show')
    }else {
        menu.classList.remove('show')
        menu.classList.add('hide')
    }
}
buttons.forEach((button) => {
    button.addEventListener('click', onClick)
})