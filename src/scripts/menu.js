let burger = document.querySelector('.header__menu')
let menu = document.querySelector('.main-menu')

burger.addEventListener('click', (e) => {
    let isHide = menu.classList.contains('hide')
    if(isHide) {
        menu.classList.remove('hide')
        menu.classList.add('show')
        burger.classList.add('show-menu')
    }else {
        menu.classList.remove('show')
        menu.classList.add('hide')
        burger.classList.remove('show-menu')
    }
})